﻿using System.ComponentModel.DataAnnotations;

namespace ConcertApp.Models
{
    public class Concert
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string ConcertName { get; set; }
        [Required]
        public DateTime ConcertDate { get; set; }
        [Required]
        public double ConcertPrice { get; set; }
        [Required]
        public string ConcertPlace { get; set; }
        public string? ConcertImageURL { get; set; }
    }
}
