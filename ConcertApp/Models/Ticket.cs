﻿using System.ComponentModel.DataAnnotations;

namespace ConcertApp.Models
{
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public int NumberOfPeople { get; set; }
        public virtual ICollection<ConcertUser>? TicketHolder { get; set; }
    }
}
